#include<stdio.h>
void qsort (int *a,int size){
    //calls recursive quicksort
    quicksort(a,0,size);
}
void quicksort(int a[],int first,int last){
    //defines iterators, piot, and holder
   int i, j, pivot, temp;
    // if the first is less than the last, the pivot and iterators are initialized
   if(first<last){
      pivot=first;
      i=first;
      j=last;
    //while iterators haven't met
      while(i<j){
          //checks that element at left iterator is less than the element pivot
         while (a[i]< a[pivot]&&i<last)
         //iterates left iterator
            i++;
        //checks that element at right iterator is greater than the element at the pivot
         while (a[j] >a[pivot])
         //iterates left iterator
            j--;
            //if the iterators havent met
         if(i<j){
            //swaps indices
            temp =a[i];
         a[i] =a[j];
         a[j]=temp;
         }
      }
      //swaps indices  
      temp = a[pivot];
     a[pivot] = a[j];
     a[j] = temp;
      //recursive call for left and right sides
      quicksort(a,first,j-1);
      quicksort (a,j+1,last);

   }
}

int main()
{
    //get size from user
    int size;
     printf("Enter size of array: ");
    scanf("%d", &size);
  int userArr[size]; // Declare a
    int i;

   

    //get elements from user
    printf("Enter %d elements in the array : ", size);
    for(i=0; i<size; i++)
    {
        scanf("%d", &userArr[i]);
    }
    //changes to pointer to meet input requirement
    int *a = userArr;
    qsort(a, size);
    int loop;
    //prints altered array
   for(loop = 0; loop <size; loop++)
      printf("%d ",userArr[loop]);
}
