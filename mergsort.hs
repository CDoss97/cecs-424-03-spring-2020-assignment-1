--defining quicksort, given that the items in the list must be orderable, we define that the list creates another list of the same type
quicksort :: Ord t => [t] -> [t]
--somewhat similar to initialization, we must define the case for when the list is empty
quicksort [] = []
--Once again, we must define when the list has one item given that haskell doesn't have null pointers
quicksort [i] = [i]
--Here is the fun stuff, where we define the quicksort algorithm! adds the left and right lists to be defined
quicksort (p:xs) = (quicksort less) ++ [p] ++ (quicksort greater)
    where
        --returns list of values less than [p]
        less = filter (< p) xs
        --returns list of values less than [p]
        greater = filter (>=p) xs
-- before we create the mergesort algorithm, lets create the merge function
-- for our merge function, we must define the three cases described earlier
merge :: Ord a => [a] -> [a] -> [a] 
merge [] ys = ys                    
merge xs [] = xs   
--defines merge for two lists, with conditions
merge (x:xs) (y:ys)
    | x<=y  = x : merge xs (y:ys)
    | otherwise = y : merge (x:xs) ys

--for merge sort, we must define the three cases described earlier
mergesort :: Ord a => [a] -> [a]
mergesort [] = []
mergesort [a] = [a]
--defines recursive call to merge function
mergesort xs=merge (lhalf) (rhalf)
    where lhalf = take((length xs) `div` 2) xs -- gets the size of the list for param 1 and list as param2
          rhalf = drop((length xs) `div` 2) xs -- drop the first half of elements 
main = do
    print (mergesort[3,4,3,23,42,1])
    print (quicksort[3,45,24,35,7,1])