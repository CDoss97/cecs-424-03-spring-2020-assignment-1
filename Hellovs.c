#include <stdio.h>
#include <stdlib.h>

void merge(int a[], int Lfirst, int mid, int end)
{
    int* sorted = (int*)malloc(sizeof(int) * (end - Lfirst + 1));
    int l = Lfirst; //left iterator
    int r = mid + 1; // right iterator (from start of right side)
    int sortedInd = 0; // index of current location in the sorting userArray
//while left and right iterators havent reached the end of sub userArrays, add the sorted amt to temp sorted userArr
    while (l <= mid && r <= end)
    {
        if (a[l] < a[r])
        {
            sorted[sortedInd] = a[l]; //already sorted, added to userArray
            l++;
        }
        else {
            sorted[sortedInd] = a[r]; // moves first element in r to the front cuz it was smaller
            r++;
        }
        sortedInd++; // increment the current position of sorted
    }

    // checks the remaining elements in the left side 
    while (l <= mid) 
    {
        sorted[sortedInd] = a[l];
        l++;
        sortedInd++;
    }
    //checks the rest of the elemets on the right side
    while (r <= end) 
    {
        sorted[sortedInd] = a[r];
        r++;
        sortedInd++;
    }//userArranges a so that the values are in the same position as the sorted userArray
    for (int i = 0; i < sortedInd; i++) {
        a[Lfirst+i] = sorted[i];
    }
    free(sorted);//free allocated space (used this becuase I kept having segmentation faults)
}


void mergesort4000(int *a, int l, int r)
{
    int mid;
    //if the iterators havent met
    if (l < r)
    {
        //gets middle
        mid = (l + r) / 2;
        mergesort4000(a, l, mid);         //left side recursion
            mergesort4000(a,mid + 1, r); //right recursion
            merge(a, l, mid, r);          //merging of two sorted sub-userArrays
    }
}
void mergesort(int *a, int size)
{
    ///call to recursive mergesort
    mergesort4000(a, 0, size);
    
  
}
int main()
{
    //get size from user
    int size;
     printf("Enter size of userArray: ");
    scanf("%d", &size);
  int userArr[size]; // Declare arr
    int i;

   

    //get elements from user
    printf("Enter %d elements in the userArray : ", size);
    for(i=0; i<size; i++)
    {
        scanf("%d", &userArr[i]);
    }
    //changes to pointer for input requirement
    int *a = userArr;
    mergesort(a, size);
    int loop;
    //prints altered array
   for(loop = 0; loop < size; loop++)
      printf("%d ",userArr[loop]);
}
